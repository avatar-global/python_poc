import unittest
from app.calculator import Calculator


class TestCalculator(unittest.TestCase):

    def test_sum(self):
        a = Calculator()
        self.assertTrue(5, a.sum(2, 3))

    def test_sum2(self):
        a = Calculator()
        self.assertTrue(6, a.sum(2, 4))


if __name__ == '__main__':
    unittest.main()
